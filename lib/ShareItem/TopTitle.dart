import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class TopTitle extends StatefulWidget implements PreferredSizeWidget{
  @override
  _TopTitleState createState() => _TopTitleState();

  @override
  // TODO: implement preferredSize
  Size get preferredSize => const Size.fromHeight(60);
}

class _TopTitleState extends State<TopTitle> {
  var listImageAsset = ["Conditioner","smartTV","washingMachine","washingMachine","refrigerator","router"];
  var listTitle = ["Air Conditioner","Smart TV","Light Bulb","Washing Machine","Refrigerator","Router"];
  var listContent = ["Voltas RG140","Samsung EX55 4K","Pjillips Hue 2","Bosch 7kg 5475","Whirlpool WR190","TP-LINK 878"];
  var room = ["LIVING ROOM", "KITCHEN", "DRAWING ROOM", "DINING ROOM","BADROOM"];
  var gameClassListName = ["CQ9", "Bg", "BBin", "BTi", "Kingpoker","Pinnacle","Pragmatic","Saba"];
  Image seasonImage = Image(image: AssetImage('images/Member/winter.png'),);
  Image bellImage = Image(image: AssetImage('images/Member/bell.png'),);
  // Text degree
  Color TabColor = Color(0xffa0aeb8);
  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil(width: 428, height: 926, allowFontScaling: true)..init(context);
    return AppBar(
            automaticallyImplyLeading: false,
            backgroundColor: Color(0xfff0f2f4),
            elevation: 0.0,
            // leading: seasonImage,
            centerTitle: false,
            title: Container(
              child: Row(
                children: <Widget>[
                  Container(
                    width: ScreenUtil().setWidth(30),
                    height: ScreenUtil().setHeight(40),
                    child: Image(image: AssetImage('images/Member/winter.png'),fit: BoxFit.fill,),
                  ),
                  Text("25° C",style: TextStyle(color: Color(0xff1062d8),fontSize: ScreenUtil(allowFontScaling: true).setSp(20),)),
                ],
              ),
            ),
            actions: <Widget>[
              Container(
                width: ScreenUtil().setWidth(40),
                height: ScreenUtil().setHeight(40),
                child: Image(image: AssetImage('images/Member/bell.png'),),
              ),
              Padding(padding: EdgeInsets.only(right: ScreenUtil().setWidth(15)),),
            ],
    );
  }
}
