import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mike/page/BPage.dart';
import 'package:flutter_mike/page/Home.dart';
import 'package:flutter_mike/page/Mail.dart';
import 'package:flutter_mike/page/Member.dart';
import 'package:flutter_mike/provider/LoginChangeNotifier.dart';
import 'package:flutter_mike/provider/MyCountChangeNotifier.dart';
import 'package:flutter_mike/provider/NowRouterIndex.dart';
import 'package:provider/provider.dart';

class BottomNavigationController extends StatefulWidget {
  BottomNavigationController({Key key, int index}) : super(key: key);

  @override
  _BottomNavigationControllerState createState() =>
      _BottomNavigationControllerState();
}

class _BottomNavigationControllerState extends State<BottomNavigationController> {
  //目前選擇頁索引值
  Color selectColor = Colors.red;
  var appBarTitles = ['首頁', '分類', '設定','股票慘賠'];
  var router = ["/home","/mail","/member","/bpage"];
  List _listPageData = [  //頁面的連結串列
    Home(),
    Mail(),
    Member(),
    BPage(),
  ];

  @override
  Widget build(BuildContext context) {
    int _currentIndex = 0; //預設值
    final routerIndex = Provider.of<MyCountChangeNotifier>(context);
      return BottomNavigationBar(
        currentIndex: routerIndex.count,//設定對應的索引值選中
        onTap: (int index){//index 表示選擇選項
          setState(() {
            _currentIndex = index;
          });
          routerIndex.increment(index);
          Navigator.pushNamed(context, router[routerIndex.count]);
        },
        fixedColor: selectColor,   //選中顏色
        type: BottomNavigationBarType.fixed,//設定底部tabs可以有多個按鈕
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text("首頁"),
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.category),
              title: Text("分類")
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
              title: Text("分類"),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            title: Text("股票慘賠"),
          ),
        ],
      );
  }
}
