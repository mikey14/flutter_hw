import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProgresPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    // TODO: implement paint
    Paint paint_0 = new Paint()
    ..color = Color.fromARGB(255, 33, 150, 243)
    ..style = PaintingStyle.stroke
    ..strokeWidth = 1;


    Path path_0 = Path();
    path_0.moveTo(size.width*0.0050000,size.height*0.1000000);
    path_0.quadraticBezierTo(size.width*0.1511000,size.height*0.3001400,size.width*0.4950000,size.height*0.3000000);
    path_0.quadraticBezierTo(size.width*0.8497500,size.height*0.2986200,size.width*0.9900000,size.height*0.1040000);

    canvas.drawPath(path_0, paint_0);

  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return true;
  }
}