import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mike/page/BPage.dart';
import 'package:flutter_mike/page/CQLogin.dart';
import 'package:flutter_mike/page/Home.dart';
import 'package:flutter_mike/page/Mail.dart';
import 'package:flutter_mike/page/Member.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class Tabs extends StatefulWidget {
  Tabs({Key key}) : super(key: key);
  _TabsState createState() => _TabsState();
}

class _TabsState extends State<Tabs> with TickerProviderStateMixin{
  var listImageAsset = ["Conditioner","smartTV","washingMachine","washingMachine","refrigerator","router"];
  var listTitle = ["Air Conditioner","Smart TV","Light Bulb","Washing Machine","Refrigerator","Router"];
  var listContent = ["Voltas RG140","Samsung EX55 4K","Pjillips Hue 2","Bosch 7kg 5475","Whirlpool WR190","TP-LINK 878"];
  var room = ["LIVING ROOM", "KITCHEN", "DRAWING ROOM", "DINING ROOM","BADROOM"];
  var gameClassListName = ["CQ9", "Bg", "BBin", "BTi", "Kingpoker","Pinnacle","Pragmatic","Saba"];
  Image seasonImage = Image(image: AssetImage('images/Member/winter.png'),);
  Image bellImage = Image(image: AssetImage('images/Member/bell.png'),);
  Text degree = Text("25° C",style: TextStyle(color: Color(0xff1062d8),fontSize: 20.0,));
  Color TabColor = Color(0xffa0aeb8);
  TabController _tabController;
  TabController _bottomTabController;
  var _currentIndex  = 0;
  var _bottomSelectedIndex = 0;
  var isToggled = false;

  // 當前頁面陣列
  List _pageList = [
    BPage(),
    Mail(),
    Member(),
    Home(),
  ];

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: room.length);
    _bottomTabController = new TabController(vsync: this, length: _pageList.length+1);
    _tabController.addListener(_handleTabSelection);
    _bottomTabController.addListener(_handleBottomTabSelection);
  }

  void _handleBottomTabSelection() {
    if (_bottomTabController.indexIsChanging) {
      setState(() {
        _bottomSelectedIndex = _bottomTabController.index;
        // print(_bottomSelectedIndex);
      });
    }
  }

  void _handleTabSelection() {
    if (_tabController.indexIsChanging) {
      setState(() {
        _currentIndex = _tabController.index;
      });
    }
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil(width: 375, height: 812, allowFontScaling: true)..init(context);
    return Scaffold(
      body:this._pageList[this.getIndex()],
      bottomNavigationBar: _getBottom(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Icon(Icons.add),
      ),
      // 设置 floatingActionButton 在底部导航栏中间
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }

  Widget _getTabBar() {
    return TabBar(
      indicatorWeight: 2,
      indicatorSize: TabBarIndicatorSize.label,
      isScrollable: true,
      labelPadding: EdgeInsets.symmetric(horizontal: 15),
      labelStyle: TextStyle(fontSize: 20.0),
      labelColor: Colors.black,
      tabs: _tabsMaker(),
      controller: _tabController,
      onTap: (int index){
        setState(() {
          _currentIndex = index;
        });},
    );
  }

  List<Tab> _tabsMaker() {
    List<Tab> tabs = []; //create an empty list of Tab
    for (var i = 0; i < room.length; i++) {
      tabs.add(Tab(child: Text(room[i],style: TextStyle(color: _currentIndex == i ?Colors.black:TabColor,fontSize: ScreenUtil(allowFontScaling: true).setSp(16),
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,),),)); //add your tabs to the list
    }
    return tabs; //return the list
  }

  Widget _getBottom() {
    return BottomAppBar(
      notchMargin: 4,
      shape: CircularNotchedRectangle(),
      child: TabBar(
        tabs: [
          IconButton(
            icon: Icon(
              Icons.home,
              size: ScreenUtil().setWidth(35),
              color: _bottomSelectedIndex == 0 ? Colors.blue : Color(
                  0xffdfe6ec),
            ),
          ),
          IconButton(
            icon: Icon(
              Icons.business,
              size: ScreenUtil(allowFontScaling: true).setSp(30),
              color: _bottomSelectedIndex == 1 ? Colors.blue : Color(
                  0xffdfe6ec),
            ),
          ),
          SizedBox(), // 增加一些间隔
          IconButton(
            icon: Icon(
              Icons.school,
              size: ScreenUtil(allowFontScaling: true).setSp(30),
              color: _bottomSelectedIndex == 3 ? Colors.blue : Color(
                  0xffdfe6ec),
            ),
          ),
          IconButton(
            icon: Icon(
              Icons.search,
              size: ScreenUtil(allowFontScaling: true).setSp(30),
              color: _bottomSelectedIndex == 4 ? Colors.blue : Color(
                  0xffdfe6ec),
            ),
          ),
        ],
        isScrollable: false,
        indicatorSize: TabBarIndicatorSize.tab,
        indicatorPadding: EdgeInsets.all(5.0),
        indicatorColor: Colors.blue,
        controller: _bottomTabController,
        indicator: UnderlineTabIndicator(
          insets: EdgeInsets.fromLTRB(15, 0.0, 15, 50),
          borderSide:
          BorderSide(color: Theme
              .of(context)
              .primaryColor, width: 2),
        ),
        onTap: (int index) {
          setState(() {
            _bottomSelectedIndex = index;
          });
        },
      ),
    );
  }
  int getIndex(){
    if(_bottomSelectedIndex>2){
      print('if');
      print(_bottomSelectedIndex - 1);
      return _bottomSelectedIndex - 1;
    }else{
      print('else');
      print(_bottomSelectedIndex);
      return _bottomSelectedIndex;
    }
  }
}