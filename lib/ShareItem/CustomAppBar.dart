import 'package:flutter/material.dart';

class CustomAppBar extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = new Path();
    path.lineTo(0, size.height) ;
    path.quadraticBezierTo(0, size.height-40, size.width/5, size.height-40) ;
    path.lineTo(size.width-40, size.height-40);
    path.quadraticBezierTo(
        size.width / 2, size.height / 2, size.width, size.height * 0.25);
    // path.quadraticBezierTo(3/4 * size.width, size.height, size.width, size.height - 20) ;

    path.lineTo(size.width, 0) ;





    return path ;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    // TODO: implement shouldReclip
    return false;
  }



}