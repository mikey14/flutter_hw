class Validate{
  static String validator(String filedName, String value) {
    if(value.isEmpty){
      return "不可為空";
    }
    switch(filedName){
      case 'username':
        return value.trim().length > 5 ? null : "帳號不能少于6位";
      case 'password':
        return value.trim().length > 6 ? null : "密碼不能少于7位";
    }
  }
}