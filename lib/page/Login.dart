import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mike/Validator/Validate.dart';
import 'package:flutter_mike/page/InitApp.dart';
import 'package:flutter_mike/provider/LoginChangeNotifier.dart';
import 'package:flutter_mike/provider/MyCountChangeNotifier.dart';
import 'package:provider/provider.dart';

import 'BPage.dart';
import 'Home.dart';
import 'Mail.dart';
import 'Member.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> with SingleTickerProviderStateMixin {
  final pages = [Home(), Mail(), Member(),BPage()];
  GlobalKey _formKey= new GlobalKey<FormState>();
  bool isclicked = false;
  TabController _tabController;
  int _selectedIndex = 0;
  TextEditingController _unameController = new TextEditingController();
  TextEditingController _pwdController = new TextEditingController();
  var LoginTextFiledTitle = ["帳號", "密碼"];
  var LoginTitleIcon= [Icon(Icons.account_circle,color: Colors.white,),Icon(Icons.lock,color: Colors.white,)];
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 2);
    _tabController.addListener(_handleTabSelection);
  }

  void _handleTabSelection() {
    setState(() {
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.menu),
          onPressed: () {},
        ),
        title: Center(child: Text('登入'),),
        actions: [
          IconButton(
            icon: Icon(Icons.email),
            onPressed: () {},
          ),
        ],
      ),
      body: GestureDetector(
        onTap: () {
          // 触摸收起键盘
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: ConstrainedBox(
          constraints: BoxConstraints.expand(),
          child: Stack(
            alignment:Alignment.center ,
            fit: StackFit.expand, //未定位widget占满Stack整个空间
            children: <Widget>[
              Container(
                color: Color(0xff0d0d0d),
              ),
              Positioned(
                top: 0,
                left: 0,
                right: 0,
                height: 220,
                child: Container(
                    decoration: new BoxDecoration(
                        color: Color(0xff1e1e1e)
                    )
                ),
              ),
              Positioned(
                top: 40,
                child: Image(image: AssetImage('images/Member/loading.png'),),
              ),
              Positioned(
                top: 140,
                left: 8,
                right: 8,
                child: Container(
                  decoration: new BoxDecoration(
                    color: Color(0xff000000),
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(
                      color: Color(0xff4b4b4b),
                      width: 1,
                    ),
                  ),
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: 40,
                        child: DefaultTabController(
                          length: 2,
                          child: TabBar(
                            controller: _tabController,
                            labelStyle: TextStyle(fontSize: 16),
                            unselectedLabelColor: Colors.white,
                            indicatorColor: Color(0xfffcd04b),
                            labelColor: Color(0xfffcd04b),
                            tabs: [
                              Text("登入",
                                  style: TextStyle(
                                    fontFamily: 'PingFangSC',
                                    color: _tabController.index == 0 ? Color(0xfffcd04b) : Color(0xffffffff),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                  )
                              ),
                              Text("註冊",
                                  style: TextStyle(
                                    fontFamily: 'PingFangSC',
                                    color: _tabController.index == 1 ? Color(0xfffcd04b) : Color(0xffffffff),
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                  )
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 10),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 5,right: 5),
                        child: Form(
                          key: _formKey,
                          autovalidate: isclicked,
                          child: Column(
                            children: <Widget>[
                              Container(
                                  child: Column(
                                    children: <Widget>[
                                      _buildUserNameTextField(),
                                      _buildPasswordTextField(),
                                    ],
                                  ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 10),
                                 child: GestureDetector(
                                   onTap: (){
                                     print('hhhhhh');
                                     _submitForm();
                                   },
                                   child: Container(
                                     height: 44,
                                     decoration: BoxDecoration(
                                       color: Color(0xfffcd04b),
                                       borderRadius: BorderRadius.circular(22),
                                     ),
                                     child: Center(
                                       child: Text("登入",style: TextStyle(color: Colors.black),),
                                     ),
                                   ),
                                 ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 50),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
  void _submitForm() {
    print('hahah');
    final login = Provider.of<LoginChangeNotifier>(context);
    setState(() {
      _formKey;
    });
    isclicked = true;
    print(_unameController.text);
    print(_pwdController.text);
    print(login.username);
    print(login.password);
    if((_formKey.currentState as FormState).validate()){
      print(_unameController.text);
      print(_pwdController.text);
      print(login.username);
      print(login.password);
      if(_unameController.text == login.username && _pwdController.text == login.password){
        print('Success');
        final routerIndex = Provider.of<MyCountChangeNotifier>(context);
        routerIndex.increment(2);
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => InitApp()));
      }

    }
  }

  Widget _buildPasswordTextField() {
    return TextFormField(
      controller: _pwdController,
      style: TextStyle(color: Color(0xffffffff)),
      obscureText: true,
      validator: (value){
        return Validate.validator('password',value);
      },
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.account_circle,color: Colors.white,),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
        ),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Color(0xff4b4b4b)),
        ),
        fillColor: Colors.black.withOpacity(0.6),
        hintText: "密碼",
        hintStyle: TextStyle(
          fontFamily: 'MicrosoftYaHei',
          color: Color(0xff707070),
          fontSize: 14,
          fontWeight: FontWeight.w400,
          fontStyle: FontStyle.normal,
        ),
      ),
    );
  }

  Widget _buildUserNameTextField() {
    return TextFormField(
      controller: _unameController,
      style: TextStyle(color: Color(0xffffffff)),
      validator: (v) {
        return Validate.validator('username',v);
      },
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(5.0),
        labelText: '請輸入帳號',
        labelStyle: TextStyle(
          fontFamily: 'MicrosoftYaHei',
          color: Color(0xffb1b1b1),
          fontSize: 14,
          fontWeight: FontWeight.w400,
          fontStyle: FontStyle.normal,
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
        ),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Color(0xff4b4b4b)),
        ),
        fillColor: Colors.black.withOpacity(0.6),
      ),
    );
  }

  // List<TextFormField> _createTextField() {
  //   List<TextFormField> textFormFiled = [];
  //   for(int i = 0;i<LoginTextFiledTitle.length;i++){
  //     var hintText = LoginTextFiledTitle[i];
  //     textFormFiled.add(TextFormField(
  //       style: TextStyle(color: Color(0xffffffff)),
  //       obscureText: true,
  //       validator: validator.getValidator('username'),
  //       decoration: InputDecoration(
  //         prefixIcon: (LoginTitleIcon[i]),
  //         focusedBorder: UnderlineInputBorder(
  //           borderSide: BorderSide(color: Colors.white),
  //         ),
  //         enabledBorder: UnderlineInputBorder(
  //           borderSide: BorderSide(color: Color(0xff4b4b4b)),
  //         ),
  //         fillColor: Colors.black.withOpacity(0.6),
  //         hintText: hintText,
  //         hintStyle: TextStyle(
  //           fontFamily: 'MicrosoftYaHei',
  //           color: Color(0xff707070),
  //           fontSize: 14,
  //           fontWeight: FontWeight.w400,
  //           fontStyle: FontStyle.normal,
  //         ),
  //       ),
  //     ));
  //   }
  //   return textFormFiled;
  // }
}
