import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class BPage extends StatefulWidget {
  @override
  _BPageState createState() => _BPageState();
}

class _BPageState extends State<BPage> with TickerProviderStateMixin {

  var listImageAsset = ["Conditioner","smartTV","smartTV","washingMachine","refrigerator","router"];
  var listTitle = ["Air Conditioner","Smart TV","Light Bulb","Washing Machine","Refrigerator","Router"];
  var listContent = ["Voltas RG140","Samsung EX55 4K","Pjillips Hue 2","Bosch 7kg 5475","Whirlpool WR190","TP-LINK 878"];
  var room = ["LIVING ROOM", "KITCHEN", "DRAWING ROOM", "DINING ROOM","BADROOM"];
  var gameClassListName = ["CQ9", "Bg", "BBin", "BTi", "Kingpoker","Pinnacle","Pragmatic","Saba"];
  Image seasonImage = Image(image: AssetImage('images/Member/winter.png'),);
  Text degree = Text("25° C",style: TextStyle(color: Color(0xff1062d8),fontSize: 20.0,));
  Color TabColor = Color(0xffa0aeb8);
  var _currentIndex  = 0;
  var isToggled = false;
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: room.length);
    _tabController.addListener(_handleTabSelection);
  }


  void _handleTabSelection() {
    if (_tabController.indexIsChanging) {
      setState(() {
        _currentIndex = _tabController.index;
      });
    }
  }


  @override
  Widget build(BuildContext context) {
    final size =MediaQuery.of(context).size;
    final width =size.width;
    final height =size.height;
    print('width is $width; height is $height');
    ScreenUtil.instance = ScreenUtil(width: 428, height: 926)..init(context);
    return Scaffold(
      backgroundColor: Color(0xfff0f2f4),//背景顏色
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(ScreenUtil().setHeight(120)),
        child: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Color(0xfff0f2f4),
          elevation: 0.0,
          // leading: seasonImage,
          centerTitle: false,
          title: Container(
            child: Row(
              children: <Widget>[
                Container(
                  width: ScreenUtil().setWidth(30),
                  height: ScreenUtil().setHeight(40),
                  child: Image(image: AssetImage('images/Member/winter.png'),fit: BoxFit.fill,),
                ),
                Text("25° C",style: TextStyle(color: Color(0xff1062d8),fontSize: ScreenUtil(allowFontScaling: true).setSp(20),)),
              ],
            ),
          ),
          actions: <Widget>[
            Container(
              width: ScreenUtil().setWidth(40),
              height: ScreenUtil().setHeight(40),
              child: Image(image: AssetImage('images/Member/bell.png'),),
            ),
            Padding(padding: EdgeInsets.only(right: ScreenUtil().setWidth(15)),),
          ],
          bottom: _getTabBar(),
        ),
      ),
      body: _getTabView(),
    );
  }

  Widget _getTabBar() {
    return TabBar(
      indicatorWeight: 2,
      indicatorSize: TabBarIndicatorSize.label,
      isScrollable: true,
      labelPadding: EdgeInsets.symmetric(horizontal: ScreenUtil(allowFontScaling: true).setSp(15)),
      labelStyle: TextStyle(fontSize: ScreenUtil(allowFontScaling: true).setSp(20)),
      labelColor: Colors.black,
      tabs: _tabsMaker(),
      controller: _tabController,
      onTap: (int index){
        setState(() {
          _currentIndex = index;
        });},
    );
  }

  List<Tab> _tabsMaker() {
    List<Tab> tabs = []; //create an empty list of Tab
    for (var i = 0; i < room.length; i++) {
      tabs.add(Tab(child: Text(room[i],style: TextStyle(color: _currentIndex == i ?Colors.black:TabColor,fontSize: ScreenUtil(allowFontScaling: true).setSp(16),
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,),),)); //add your tabs to the list
    }
    return tabs; //return the list
  }

  Widget _getTabView() {
    switch(_currentIndex){
      case 0:
        return Container(
          padding: EdgeInsets.only(right: 20,left: 20,top: 30),
                  child: GridView.builder(
                    shrinkWrap: true,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 25,
                      mainAxisSpacing: 25,
                      childAspectRatio: 1.08,//主軸比例/縱軸比例
                    ),
                    itemCount: listTitle.length,
                    itemBuilder: _tabListMaker,
                  ),
            );
        break;

      default:
        print('no');
        return Container(
          height: 100,
          width: 100,
          color: Colors.green,
        );
        break;
    }

  }

  Widget _tabListMaker(BuildContext context, int index) {
    if(index == 2){
      return Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(colors:[Color(0xff0070db),Color(0xff1896ed)]), //背景渐变
            color: Colors.white,
            borderRadius: BorderRadius.circular(20.0),
          ),
          child: Container(
            padding: EdgeInsets.only(left: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        width: ScreenUtil().setWidth(40),
                        height: ScreenUtil().setHeight(40),
                        child: Image(image: AssetImage("images/Member/"+listImageAsset[index]+".png"),fit: BoxFit.fill,),
                      ),
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(listTitle[index],style: TextStyle(fontSize: ScreenUtil(allowFontScaling: true).setSp(16),color: Colors.white),),
                            Padding(padding: EdgeInsets.only(top: 5),),
                            Text(listContent[index],style: TextStyle(fontSize: ScreenUtil(allowFontScaling: true).setSp(12),color: Colors.white),),
                          ],
                        ),
                      ),
                      FlutterSwitch(
                        height: ScreenUtil().setWidth(20),
                        width: ScreenUtil().setWidth(40),
                        padding: 2.0,
                        toggleSize: ScreenUtil().setWidth(15),
                        borderRadius: ScreenUtil().setWidth(10),
                        circleColor: Colors.white,
                        borderColor: Colors.white,
                        inactiveColor:Colors.yellow,
                        value: isToggled,
                        onToggle: (value) {
                          setState(() {
                            isToggled = value;
                          });
                        },
                      ),
                    ],
                  ),
                ),
                InkWell(
                  child: Center(
                    child: Container(
                      width: ScreenUtil().setWidth(100),
                      decoration: new BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                              color: Color(0xff92c5ec),
                              blurRadius: 15.0,
                              offset: Offset(0, 6),
                              spreadRadius: 8 //陰影擴散程度
                          )
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          )
      );
    }
    return Container(
        decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20.0),
              ),
      child: Container(
        padding: EdgeInsets.only(left: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: ScreenUtil().setWidth(40),
              height: ScreenUtil().setHeight(40),
              child: Image(image: AssetImage("images/Member/"+listImageAsset[index]+".png"),fit: BoxFit.fill,),
            ),
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(listTitle[index],style: TextStyle(fontSize: ScreenUtil().setSp(18),color: Colors.black,),),
                  Padding(padding: EdgeInsets.only(top: 5),),
                  Text(listContent[index],style: TextStyle(fontSize: ScreenUtil(allowFontScaling: true).setSp(14),color: Color(0xff8b9caa)),),
                ],
              ),
            ),
            FlutterSwitch(
              height: ScreenUtil().setWidth(20),
              width: ScreenUtil().setWidth(40),
              padding: 2.0,
              toggleSize: ScreenUtil().setWidth(15),
              borderRadius: ScreenUtil().setWidth(10),
              circleColor: Color(0xff006cd9),
              borderColor: Color(0xff006cd9),
              // activeColor: Colors.blue,
              // inactiveColor:Colors.blue,
              value: isToggled,
              onToggle: (value) {
                setState(() {
                  isToggled = value;
                });
              },
            ),
          ],
        ),
      )
    );
  }
}
