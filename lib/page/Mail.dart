import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mike/ShareItem/BottomNavigationController.dart';
import 'package:flutter_mike/ShareItem/TopTitle.dart';

// class Mail extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//
//   }
// }
class Mail extends StatefulWidget {
  @override
  _MailState createState() => _MailState();
}

class _MailState extends State<Mail> {
  var gameClassTilte = ["体育电竞", "真人", "电子", "P2P", "彩票"];
  var gameClassListName = ["CQ9", "Bg", "BBin", "BTi", "Kingpoker","Pinnacle","Pragmatic","Saba"];
  FocusNode focusNode1 = new FocusNode();
  final searchTextController = TextEditingController();
  Image searchIcon = Image(image: AssetImage("images/Member/icon-search-n@2x.png"),width: 30,height: 30,);
  Color stringColor = Colors.white;
  bool focusFlag = true;

  @override
  void initState() {
    focusNode1.addListener((){
      if(focusFlag) {
        if (focusNode1.hasFocus) {
          setState(() {
            searchIcon =
                Image(image: AssetImage("images/Member/icon-search@3x.png"),);
          });
        } else {
          setState(() {
            searchIcon =
                Image(image: AssetImage("images/Member/icon-search-n@2x.png"),);
          });
        }
      }
    });
  }
  @override
  Widget build(BuildContext context) {
    final size =MediaQuery.of(context).size;
       return Scaffold(
         appBar: TopTitle(),
         body: GestureDetector(
           onTap: () {
             // 触摸收起键盘
             FocusScope.of(context).requestFocus(FocusNode());
           },
           child: Scrollbar(
             child: SingleChildScrollView(
               scrollDirection: Axis.vertical,
               child: Container(
                 width: double.infinity,
                 color: Color(0xff0d0d0d),
                 child: Column(
                   crossAxisAlignment: CrossAxisAlignment.start,
                   children: <Widget>[
                     Container(
                       padding: EdgeInsets.only(left: 15, top: 16),
                       child: (Text("中心钱包余额",
                           style: TextStyle(
                             fontFamily: 'PingFangSC',
                             color: stringColor,
                             fontSize: 12,
                             fontWeight: FontWeight.w400,
                             fontStyle: FontStyle.normal,
                           ))),
                     ),
                     Container(
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                         children: <Widget>[
                           Container(
                             padding: EdgeInsets.only(left: 15),
                             child: RichText(
                               text: new TextSpan(
                                 children: [
                                   new TextSpan(
                                       text: "฿",
                                       style: TextStyle(
                                         fontFamily: 'Thonburi',
                                         color: Color(0xfffcd04b),
                                         fontSize: 20,
                                         fontWeight: FontWeight.w400,
                                         fontStyle: FontStyle.normal,
                                       )),
                                   new TextSpan(
                                       text: "123,456,789.00",
                                       style: TextStyle(
                                         fontFamily: 'PingFangSC',
                                         color: Color(0xfffcd04b),
                                         fontSize: 20,
                                         fontWeight: FontWeight.w400,
                                         fontStyle: FontStyle.normal,
                                       )),
                                 ],
                               ),
                             ),
                           ),
                           Container(
                             padding: EdgeInsets.only(right: 15),
                             child: Row(
                               children: <Widget>[
                                 Container(
                                   child: Center(
                                     child: Text(
                                       "户内转账",
                                       style: TextStyle(
                                         fontFamily: 'HelveticaNeue',
                                         color: Color(0xfffcd04b),
                                         fontSize: 12,
                                         fontWeight: FontWeight.w400,
                                         fontStyle: FontStyle.normal,
                                       ),
                                     ),
                                   ),
                                   height: 24,
                                   width: 80,
                                   decoration: BoxDecoration(
                                     border: Border.all(
                                       color: Color(0xfffcd04b),
                                       width: 1,
                                     ),
                                     borderRadius: BorderRadius.circular(22),
                                   ),
                                 ),
                                 Padding(
                                   padding: EdgeInsets.only(right: 6),
                                 ),
                                 Container(
                                     child: Center(
                                       child: Text("充值",
                                         style: TextStyle(
                                           fontFamily: 'HelveticaNeue',
                                           color: Colors.black,
                                           fontSize: 12,
                                           fontWeight: FontWeight.w400,
                                           fontStyle: FontStyle.normal,
                                         ),),
                                     ),
                                     width: 80,
                                     height: 24,
                                     decoration: new BoxDecoration(
                                         color: Color(0xfffcd04b),
                                         borderRadius: BorderRadius.circular(22)
                                     )
                                 ),
                               ],
                             ),
                           )
                         ],
                       ),
                     ),
                     Container(
                       child: DefaultTabController(
                         length: gameClassTilte.length,
                         child: TabBar(
                           labelStyle: TextStyle(fontSize: 20.0),
                           unselectedLabelColor: Colors.grey,
                           indicatorColor: Color(0xfffcd04b),
                           labelColor: Colors.black,
                           tabs: _tabsMaker(),
                         ),
                       ),
                     ),
                     Container(
                       height: 48,
                       decoration: BoxDecoration(
                         color: Color(0xff0d0d0d),
                       ),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                         children: <Widget>[
                           Container(
                             padding: EdgeInsets.only(left: 15),
                             width:300,//todo
                             child: TextField(
                               onChanged: (value){
                                 if(value.length > 0){
                                   focusFlag = false;
                                   initGameClass();
                                   gameClassListName = gameClassListName.where((f) => f.toLowerCase().startsWith(value.toLowerCase())).toList();
                                   setState(() {
                                     _gameListMaker;
                                     searchIcon = Image(image: AssetImage("images/Member/icon-close@2x.png"),);
                                   });
                                 }else{
                                   initGameClass();
                                   focusFlag = true;
                                   setState(() {
                                     searchIcon = Image(image: AssetImage("images/Member/icon-search-n@2x.png"),);
                                   });
                                 }
                               },
                               // autofocus: true,
                               focusNode: focusNode1,
                               controller: searchTextController,
                               style: TextStyle(color: Color(0xffffffff)),
                               decoration: InputDecoration(
                                 focusedBorder: UnderlineInputBorder(
                                   borderSide: BorderSide(color: Colors.white),
                                 ),
                                 fillColor: Colors.black.withOpacity(0.6),
                                 hintText: "搜尋遊戲",
                                 hintStyle: TextStyle(
                                   fontFamily: 'MicrosoftYaHei',
                                   color: Color(0xff707070),
                                   fontSize: 14,
                                   fontWeight: FontWeight.w400,
                                   fontStyle: FontStyle.normal,
                                 ),
                               ),
                             ),
                           ),
                           GestureDetector(
                             onTap: (){
                               setState(() {
                                 initGameClass();
                                 _gameListMaker;
                                 searchIcon = Image(image: AssetImage("images/Member/icon-search-n@2x.png"),);
                                 searchTextController.text = "";
                               });
                             },
                             child: Container(
                               width: 30,
                               height: 30,
                               padding: EdgeInsets.only(right: 15),
                               child: searchIcon,
                             ),
                           )
                         ],
                       ),
                     ),
                     Container(
                         padding: EdgeInsets.only(top: 10,right: 8,left: 8),
                         child: GridView.builder(
                           shrinkWrap: true,
                           physics: NeverScrollableScrollPhysics(),
                           gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                             crossAxisCount: 2,
                             crossAxisSpacing: 10,
                             mainAxisSpacing: 10,
                             childAspectRatio: 1.59,//主軸比例/縱軸比例
                           ),
                           itemCount: gameClassListName.length,
                           // shrinkWrap: true,
                           itemBuilder: _gameListMaker,
                         )
                     ),
                   ],
                 ),
               ),
             ),
           ),
         ),
       );
  }

  List<Tab> _tabsMaker() {
    List<Tab> tabs = []; //create an empty list of Tab
    for (var i = 0; i < gameClassTilte.length; i++) {
      tabs.add(Tab(child: Text(gameClassTilte[i],style: TextStyle(color: Color(0xffebebeb),fontSize: 12,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,),),)); //add your tabs to the list
    }
    return tabs; //return the list
  }


  Widget _gameListMaker(BuildContext context, int index) {
    return Container(
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            child: Image(image: AssetImage('images/Member/slots-menu-bg.png'),fit: BoxFit.fill,),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image(image: AssetImage("images/Member/menu-provider-"+gameClassListName[index]+"@2x.png"),width: 120,height: 32,),
              Padding(
                padding: EdgeInsets.only(top: 6),
                child: Text(gameClassListName[index],style: TextStyle(color: Colors.white),),
              )
            ],
          )
        ],
      ),
    );
  }

  void initGameClass() {
    gameClassListName = ["CQ9", "Bg", "BBin", "BTi", "Kingpoker","Pinnacle","Pragmatic","Saba"];
  }
}

