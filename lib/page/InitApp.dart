import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mike/page/BPage.dart';
import 'package:flutter_mike/page/CQLogin.dart';
import 'package:flutter_mike/page/Home.dart';
import 'package:flutter_mike/page/Mail.dart';
import 'package:flutter_mike/page/Member.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class InitApp extends StatefulWidget {
  InitApp({Key key}) : super(key: key);
  _InitAppState createState() => _InitAppState();
}

class _InitAppState extends State<InitApp> with TickerProviderStateMixin{
  var listImageAsset = ["Conditioner","smartTV","washingMachine","washingMachine","refrigerator","router"];
  var listTitle = ["Air Conditioner","Smart TV","Light Bulb","Washing Machine","Refrigerator","Router"];
  var listContent = ["Voltas RG140","Samsung EX55 4K","Pjillips Hue 2","Bosch 7kg 5475","Whirlpool WR190","TP-LINK 878"];
  var room = ["LIVING ROOM", "KITCHEN", "DRAWING ROOM", "DINING ROOM","BADROOM"];
  var gameClassListName = ["CQ9", "Bg", "BBin", "BTi", "Kingpoker","Pinnacle","Pragmatic","Saba"];
  Image seasonImage = Image(image: AssetImage('images/Member/winter.png'),);
  Image bellImage = Image(image: AssetImage('images/Member/bell.png'),);
  Text degree = Text("25° C",style: TextStyle(color: Color(0xff1062d8),fontSize: 20.0,));
  Color TabColor = Color(0xffa0aeb8);
  TabController _tabController;
  TabController _bottomTabController;
  var _currentIndex  = 0;
  var _bottomSelectedIndex = 0;
  var isToggled = false;

  // 當前頁面陣列
  List _pageList = [
    BPage(),
    Mail(),
    Member(),
    Home(),
  ];

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: room.length);
    _bottomTabController = new TabController(vsync: this, length: _pageList.length+1);
    _tabController.addListener(_handleTabSelection);
    _bottomTabController.addListener(_handleBottomTabSelection);
  }

  void _handleBottomTabSelection() {
    if (_bottomTabController.indexIsChanging) {
      setState(() {
        _bottomSelectedIndex = _bottomTabController.index;
        // print(_bottomSelectedIndex);
      });
    }
  }

  void _handleTabSelection() {
    if (_tabController.indexIsChanging) {
      setState(() {
        _currentIndex = _tabController.index;
      });
    }
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil(width: 428.0, height: 926, allowFontScaling: true)..init(context);
    return Scaffold(
      body:this._pageList[this.getIndex()],
      bottomNavigationBar: _getBottom(),
      floatingActionButton: Container(
        height: ScreenUtil().setHeight(90),
        width: ScreenUtil().setWidth(60),
        child: FloatingActionButton(
          onPressed: (){},
          child: Icon(Icons.add,size: ScreenUtil().setWidth(30),),
        ),
      ),
      // 设置 floatingActionButton 在底部导航栏中间
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }

  Widget _getBottom() {
    return BottomAppBar(
      notchMargin: ScreenUtil().setWidth(4),
      shape: CircularNotchedRectangle(),
      child: Container(
        height: ScreenUtil().setWidth(50),
        child: TabBar(
          tabs: [
            Center(
              child: IconButton(
                icon: Icon(
                  Icons.access_alarms,
                  size: ScreenUtil().setWidth(25),
                  color: _bottomSelectedIndex == 0 ? Colors.blue : Color(
                      0xffdfe6ec),
                ),
              ),
            ),
            Center(
              child: IconButton(
                icon: Icon(
                  Icons.menu,
                  size: ScreenUtil().setWidth(25),
                  color: _bottomSelectedIndex == 1 ? Colors.blue : Color(
                      0xffdfe6ec),
                ),
              ),
            ),
            SizedBox(), // 增加一些间隔
            Center(
              child: IconButton(
                icon: Icon(
                  Icons.school,
                  size: ScreenUtil().setWidth(25),
                  color: _bottomSelectedIndex == 3 ? Colors.blue : Color(
                      0xffdfe6ec),
                ),
              ),
            ),
            Center(
              child: IconButton(
                icon: Icon(
                  Icons.mail,
                  size: ScreenUtil().setWidth(25),
                  color: _bottomSelectedIndex == 4 ? Colors.blue : Color(
                      0xffdfe6ec),
                ),
              ),
            ),
          ],
          isScrollable: false,
          indicatorSize: TabBarIndicatorSize.tab,
          indicatorPadding: EdgeInsets.all(8),
          indicatorColor: Colors.blue,
          controller: _bottomTabController,
          indicator: UnderlineTabIndicator(
            insets: EdgeInsets.fromLTRB(ScreenUtil().setWidth(15), 0.0, ScreenUtil().setWidth(15), ScreenUtil().setWidth(50)),
            borderSide:
            BorderSide(color: Theme
                .of(context)
                .primaryColor, width: 2),
          ),
          onTap: (int index) {
            setState(() {
              _bottomSelectedIndex = index;
            });
          },
        ),
      )
    );
  }
  int getIndex(){
    if(_bottomSelectedIndex>2){
      print('if');
      print(_bottomSelectedIndex - 1);
      return _bottomSelectedIndex - 1;
    }else{
      print('else');
      print(_bottomSelectedIndex);
      return _bottomSelectedIndex;
    }
  }
}