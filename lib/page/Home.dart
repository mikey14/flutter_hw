import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mike/PathTest/PathPainter.dart';
import 'package:flutter_mike/ShareItem/BottomNavigationController.dart';
import 'package:flutter_mike/ShareItem/CustomAppBar.dart';
import 'package:flutter_mike/ShareItem/Progress.dart';
import 'package:flutter_mike/ShareItem/TopTitle.dart';
import 'package:flutter_mike/page/Member.dart';
import 'package:flutter_mike/provider/MyCountChangeNotifier.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

import 'BPage.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  AnimationController animationController;
  Animation<double> animation;

  @override
  void initState() {
    super.initState();
    _setupAnimation();
  }

  _setupAnimation() {
    animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 1));
    animation = Tween(begin: 50.0, end: 150.0).animate(animationController);
  }
  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil(width: 428, height: 926)..init(context);
    final size =MediaQuery.of(context).size;
    final counter = Provider.of<MyCountChangeNotifier>(context);
    return Stack(
      alignment:Alignment.topCenter ,
      children: <Widget>[
        Container(
          child: TopTitle(),
        ),
        Container(
          width: 200,
          height: 500,
          decoration: new BoxDecoration(
            //背景
            color: Colors.red,
            //设置四周圆角 角度
            borderRadius: BorderRadius.vertical(bottom: Radius.circular(200.0)),
          ),
          child: Text("Container 的圆角边框"),
        ),
        Positioned(
          top: 400,
          child: Container(
              width: 250,
              height: 500,
              child: CustomPaint(
                painter: ProgresPainter(
                ),
              ),
          ),
        ),
        Positioned(
          top: 450,
          child: Container(),
        )


      ],
    );
  }
}


