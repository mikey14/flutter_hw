import 'package:flutter/material.dart';
import 'package:flutter_mike/ShareItem/BottomNavigationController.dart';
import 'package:flutter_mike/ShareItem/TopTitle.dart';
class Member extends StatelessWidget {
  // final member = ["自助优惠","提款银行","资金明细","站内信","账户详情","修改密码","帮助中心","代理加盟"];
  List<String> member = [
    "自助优惠",
    "提款银行",
    "资金明细",
    "站内信",
    "账户详情",
    "修改密码",
    "帮助中心",
    "代理加盟",
  ];
  List<String> memberAsset = [
    "coupon",
    "bank",
    "funddetail",
    "message",
    "account",
    "password",
    "help",
    "agent",
  ];

  @override
  Widget build(BuildContext context) {
     return Scaffold(
       appBar: TopTitle(),
       body: Scrollbar(
         child: SingleChildScrollView(
           scrollDirection: Axis.vertical,
           child: Container(
             //最外層
             color: Color.fromARGB(255, 26, 26, 26),
             child: Column(
               //水平排列
               children: <Widget>[
                 Stack(
                   //字要在bg@3x上
                   children: <Widget>[
                     SizedBox(
                       child: Container(
                         height: 210,
                         width: double.infinity, //寬度無限
                         child: Image(
                             image: AssetImage("images/Member/bg@3x.png"),
                             fit: BoxFit.fitWidth),
                       ),
                     ),
                     Container(
                       padding: EdgeInsets.only(left: 50, top: 40),
                       child: Column(
                         children: <Widget>[
                           Container(
                             height: 25,
                             child: Text(
                               "Hi",
                               style: TextStyle(fontSize: 16, color: Colors.white),
                             ),
                           ),
                           Container(
                             height: 25,
                             child: Text(
                               "abc1234",
                               style: TextStyle(fontSize: 14, color: Colors.white),
                             ),
                           ),
                           Container(
                             child: Text(
                               "至尊級",
                               style: TextStyle(fontSize: 14, color: Colors.white),
                             ),
                           ),
                         ],
                       ),
                     ),
                     Container(
                       padding: EdgeInsets.only(left: 251, top: 30),
                       child: Column(
                         children: <Widget>[
                           Container(
                             width: 60,
                             height: 60,
                             child: Image(
                               image: AssetImage(
                                   "images/Member/vip-gold-122-122@2x.png"),
                             ),
                           ),
                           Container(
                             padding: EdgeInsets.all(8.0),
                             child: Text("VIP 俱樂部",
                                 style:
                                 TextStyle(fontSize: 14, color: Colors.white)),
                           ),
                         ],
                       ),
                     ),
                     Container(
                       //中心錢包BG
                       padding: EdgeInsets.only(left: 20, top: 160, right: 20),
                       child: Container(
                         height: 98,
                         width: double.infinity,
                         color: Color.fromARGB(200, 0, 0, 0),
                         child: Stack(
                           children: <Widget>[
                             Column(
                               children: <Widget>[
                                 Container(
                                   width: double.infinity,
                                   alignment: Alignment.topCenter,
                                   padding: EdgeInsets.only(top: 16),
                                   child: Text("355,362,622.00",
                                       style: TextStyle(
                                           fontSize: 14, color: Colors.white)),
                                 ),
                                 Container(
                                   alignment: Alignment.topCenter,
                                   width: double.infinity,
                                   padding: EdgeInsets.only(top: 1),
                                   child: Text("中心钱包余额",
                                       style: TextStyle(
                                           fontSize: 14, color: Colors.white)),
                                 ),
                               ],
                             ),
                           ],
                         ),
                       ),
                     ),
                     Container(
                       padding: EdgeInsets.only(left: 20, top: 200, right: 20),
                       child: Container(
                         height: 98,
                         width: double.infinity,
                         child: Row(
                           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                           children: <Widget>[
                             Container(
                               decoration: BoxDecoration(
                                 image: DecorationImage(
                                   image: AssetImage(
                                       "images/Member/bg-mainfunction@2x.png"),
                                 ),
                               ),
                               child: (Image(
                                 image: AssetImage("images/Member/icon-help.png"),
                                 width: 60,
                                 height: 60,
                               )),
                             ),
                             Container(
                               decoration: BoxDecoration(
                                 image: DecorationImage(
                                   image: AssetImage(
                                       "images/Member/bg-mainfunction@2x.png"),
                                 ),
                               ),
                               child: (Image(
                                 image: AssetImage("images/Member/icon-help.png"),
                                 width: 60,
                                 height: 60,
                               )),
                             ),
                             Container(
                               decoration: BoxDecoration(
                                 image: DecorationImage(
                                   image: AssetImage(
                                       "images/Member/bg-mainfunction@2x.png"),
                                 ),
                               ),
                               child: (Image(
                                 image: AssetImage("images/Member/icon-help.png"),
                                 width: 60,
                                 height: 60,
                               )),
                             ),
                             // Image(image: AssetImage("images/Member/bg-mainfunction@2x.png"),width: 60,height: 60,),
                             // Image(image: AssetImage("images/Member/bg-mainfunction@2x.png"),width: 60,height: 60,),
                             // Image(image: AssetImage("images/Member/bg-mainfunction@2x.png"),width: 60,height: 60,),
                           ],
                         ),
                       ),
                     ),
                     Container(
                       padding: EdgeInsets.only(top: 280, bottom: 30),
                       child: Row(
                         children: <Widget>[
                           Container(
                             padding: EdgeInsets.only(left: 85),
                             child: Text("充值",
                                 style:
                                 TextStyle(fontSize: 14, color: Colors.white)),
                           ),
                           Container(
                             padding: EdgeInsets.only(left: 80),
                             child: Text("提現",
                                 style:
                                 TextStyle(fontSize: 14, color: Colors.white)),
                           ),
                           Container(
                             padding: EdgeInsets.only(left: 65),
                             child: Text("戶內轉帳",
                                 style:
                                 TextStyle(fontSize: 14, color: Colors.white)),
                           ),
                         ],
                       ),
                     ),
                   ],
                 ),
                 Container(
                   height: 50,
                   child: (Text("点击收合所有余额",
                       style: TextStyle(fontSize: 14, color: Colors.white))),
                 ),
                 Container(
                   child: Container(
                     height:390,//待解
                     child: ListView.builder(
                       shrinkWrap: true,
                       physics: NeverScrollableScrollPhysics(),
                       //不讓ListView也滾動，因為外層已經有設ScrollView
                       itemCount: member.length,
                       itemExtent: 55,
                       itemBuilder: (BuildContext context, int index) {
                         var name = member[index];
                         return Container(
                           padding: EdgeInsets.all(15.0),
                           child: Row(
                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
                             children: <Widget>[
                               Container(
                                 child: Row(
                                   children: <Widget>[
                                     Container(
                                       child: (Image(
                                         image: AssetImage("images/Member/icon-" +
                                             memberAsset[index] +
                                             ".png"),
                                       )),
                                     ),
                                     Container(
                                       padding: EdgeInsets.only(left: 10.0),
                                       child: (Text(
                                         name,
                                         style: TextStyle(
                                             color: Colors.white, fontSize: 16),
                                       )),
                                     ),
                                   ],
                                 ),
                               ),
                               Container(
                                 width: 14,
                                 height: 14,
                                 child: (Image(
                                   image: AssetImage(
                                       "images/Member/icon-enter@2x.png"),
                                 )),
                               ),
                             ],
                           ),
                           // 下边框
                           decoration: BoxDecoration(
                               border: Border(
                                   bottom: BorderSide(
                                       width: 1, color: Color((0xff1f1f1f))))),
                         );
                       },
                     ),
                   ),
                 ),
               ],
             ),
           ),
         ),
       ),
     );
  }
}
