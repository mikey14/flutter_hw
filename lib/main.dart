import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_mike/ShareItem/TopTitle.dart';
import 'package:flutter_mike/ShareItem/TopTitleView.dart';
import 'package:flutter_mike/page/BPage.dart';
import 'package:flutter_mike/page/CQLogin.dart';
import 'package:flutter_mike/page/Home.dart';
import 'package:flutter_mike/page/InitApp.dart';
import 'package:flutter_mike/page/Login.dart';
import 'package:flutter_mike/page/Member.dart';
import 'package:flutter_mike/page/Mail.dart';

import 'package:flutter_mike/provider/MyCountChangeNotifier.dart';
import 'package:flutter_mike/provider/LoginChangeNotifier.dart';
import 'package:flutter_mike/provider/NowRouterIndex.dart';
import 'package:provider/provider.dart';

import 'ShareItem/BottomNavigationController.dart';
import 'ShareItem/Tabs.dart';
void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: MyCountChangeNotifier()),
        ChangeNotifierProvider.value(value: LoginChangeNotifier()),
        ChangeNotifierProvider.value(value: NowRouterIndex()),
      ],
      child: MaterialApp(
        home: InitApp(),//進入點
        routes: {
          '/login':(BuildContext context) => Login(),
        },
      ),
    );
  }

}


