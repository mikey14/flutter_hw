import 'package:flutter/foundation.dart';

class NowRouterIndex with ChangeNotifier {

  // 設定一個整數私有變數 _count的欄位，初值為零
  int _nowNavBarIndex = 0;

  int selected_period = 0;

  //可以透過 Consumer 來獲得當下 count 值
  int get nowNavRouter => _nowNavBarIndex;

  //當點擊右下角＋ 浮動按鈕，會呼叫此方法
  //此方法會將 _count 累加 1，並叫 notifyListeners
  increment() {
    _nowNavBarIndex++;
    notifyListeners();
  }
}